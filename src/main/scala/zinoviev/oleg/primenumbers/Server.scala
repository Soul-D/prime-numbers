package zinoviev.oleg.primenumbers

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import zinoviev.oleg.primenumbers.services.PrimeNumberGeneratorService
import zinoviev.oleg.primenumbers.utils.LoggerLike
import zinoviev.oleg.primenumbers.web.PrimeGeneratorRestApi

class Server()(implicit val system: ActorSystem, materializer: ActorMaterializer) extends LoggerLike {
  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  def run(): Unit = {
    logger.info("Starting server...")

    val generatorService = PrimeNumberGeneratorService()
    val api = PrimeGeneratorRestApi(generatorService)

    Http().bindAndHandle(api.routes, host, port)

    logger.info("Server started at the address {}:{}", host, port)
  }
}

object Server {
  implicit val system: ActorSystem = ActorSystem("Server")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  def main(args: Array[String]): Unit =
    new Server().run()
}
