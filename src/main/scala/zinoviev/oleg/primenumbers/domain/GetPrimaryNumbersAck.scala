package zinoviev.oleg.primenumbers.domain

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class GetPrimaryNumbersAck(result: List[Int])

object GetPrimaryNumbersAck {
  implicit val getPrimaryNumbersAckFmt: Format[GetPrimaryNumbersAck] =
    (__ \ "result").format[List[Int]].inmap(GetPrimaryNumbersAck.apply, unlift(GetPrimaryNumbersAck.unapply))
}
