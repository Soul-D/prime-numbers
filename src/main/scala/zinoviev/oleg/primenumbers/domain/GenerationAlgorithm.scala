package zinoviev.oleg.primenumbers.domain

import enumeratum.{Enum, EnumEntry}
import play.api.libs.json._

import scala.annotation.tailrec
import scala.collection.immutable
import scala.collection.mutable.ListBuffer

sealed trait GenerationAlgorithm extends EnumEntry.Lowercase {
  def generatePrimes(upperBound: Int): List[Int]
}

object GenerationAlgorithm extends Enum[GenerationAlgorithm] {

  case object Eratosthenes extends GenerationAlgorithm {
    override def generatePrimes(upperBound: Int): List[Int] = {
      val isPrime = collection.mutable.BitSet(2 to upperBound: _*) -- (4 to upperBound by 2)
      for (p <- 2 +: (3 to Math.sqrt(upperBound).toInt by 2) if isPrime(p))
        isPrime --= p * p to upperBound by p
      isPrime.toList
    }
  }

  case object Recursive extends GenerationAlgorithm {
    override def generatePrimes(upperBound: Int): List[Int] = {
      def isPrime(num: Int, factors: List[Int]): Boolean = factors.forall(num % _ != 0)

      @tailrec
      def loop(i: Int, primes: List[Int]): List[Int] =
        if (i >= upperBound) primes
        else if (isPrime(i, primes)) loop(i + 1, i :: primes)
        else loop(i + 1, primes)

      loop(2, List()).reverse
    }
  }

  case object Iterative extends GenerationAlgorithm {
    override def generatePrimes(upperBound: Int): List[Int] = {
      def isPrime(num: Int, factors: Traversable[Int]): Boolean =
        factors.takeWhile(_ <= math.sqrt(num).toInt) forall (num % _ != 0)

      val primes = ListBuffer(2)

      for (i <- 3 to upperBound) {
        if (isPrime(i, primes)) {
          primes += i
        }
      }
      primes.toList
    }
  }

  implicit val algorithmTypeFmt: Format[GenerationAlgorithm] = new Format[GenerationAlgorithm] {
    override def reads(json: JsValue): JsResult[GenerationAlgorithm] = json match {
      case JsString(name) =>
        GenerationAlgorithm
          .withNameInsensitiveOption(name)
          .map(JsSuccess(_))
          .getOrElse(JsError(s"Unknown AlgorithmType type.($name)"))
      case _ => JsError(s"Wrong AlgorithmType  type format.")
    }

    override def writes(o: GenerationAlgorithm): JsValue = JsString(o.entryName)
  }

  override val values: immutable.IndexedSeq[GenerationAlgorithm] = findValues
}
