package zinoviev.oleg.primenumbers.domain

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class GetPrimaryNumbers(upperBound: Int, algorithm: Option[GenerationAlgorithm] = None)

object GetPrimaryNumbers {
  implicit val getPrimaryNumbersFmt: Format[GetPrimaryNumbers] = (
    (__ \ "upperBound").format[Int] and
      (__ \ "algorithm").formatNullable[GenerationAlgorithm]
  )(GetPrimaryNumbers.apply, unlift(GetPrimaryNumbers.unapply))
}
