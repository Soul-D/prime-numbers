package zinoviev.oleg.primenumbers.web

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import zinoviev.oleg.primenumbers.domain.GetPrimaryNumbers
import zinoviev.oleg.primenumbers.services.PrimeNumberGeneratorServiceLike
import zinoviev.oleg.primenumbers.utils.LoggerLike

class PrimeGeneratorRestApi(generatorService: PrimeNumberGeneratorServiceLike)(implicit val system: ActorSystem)
    extends Directives
    with LoggerLike
    with DirectivesHelper
    with PlayJsonSupport {
  val routes: Route =
    logRequestResponse() {
      handleRejections(rejectionHandler) {
        handleExceptions(exHandler) {
          extractExecutionContext { implicit ec =>
            pathEndOrSingleSlash {
              (post & entity(as[GetPrimaryNumbers])) { request =>
                onSuccess(generatorService.generate(request))(completeEither)
              }
            }
          }
        }
      }
    }
}

object PrimeGeneratorRestApi {
  def apply(generatorService: PrimeNumberGeneratorServiceLike)(implicit system: ActorSystem) =
    new PrimeGeneratorRestApi(generatorService)
}
