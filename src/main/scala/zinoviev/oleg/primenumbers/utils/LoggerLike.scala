package zinoviev.oleg.primenumbers.utils

import akka.actor.ActorSystem
import akka.event.{LogSource, Logging, LoggingAdapter}

trait LoggerLike {

  implicit def system: ActorSystem

  implicit val logSource: LogSource[AnyRef] = new LogSource[AnyRef] {
    def genString(o: AnyRef): String = o.getClass.getName
    override def getClazz(o: AnyRef): Class[_] = o.getClass
  }

  val logger: LoggingAdapter = Logging(system, this)
}
