package zinoviev.oleg.primenumbers.services

sealed trait PrimeNumberGeneratorServiceError { def message: String }

case class IllegalUpperBoundError(upperBound: Int) extends PrimeNumberGeneratorServiceError {
  override val message: String = s"Impossible to calculate primes for $upperBound"
}
