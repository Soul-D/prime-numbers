package zinoviev.oleg.primenumbers.services

import akka.actor.ActorSystem
import akka.http.scaladsl.util.FastFuture
import zinoviev.oleg.primenumbers.domain.GenerationAlgorithm.Iterative
import zinoviev.oleg.primenumbers.domain.{GetPrimaryNumbers, GetPrimaryNumbersAck}
import zinoviev.oleg.primenumbers.utils.LoggerLike

import scala.concurrent.{ExecutionContext, Future}

trait PrimeNumberGeneratorServiceLike {
  def generate(request: GetPrimaryNumbers)(
    implicit ec: ExecutionContext
  ): Future[Either[PrimeNumberGeneratorServiceError, GetPrimaryNumbersAck]]
}

class PrimeNumberGeneratorService()(implicit val system: ActorSystem) extends PrimeNumberGeneratorServiceLike with LoggerLike {

  override def generate(
    request: GetPrimaryNumbers
  )(implicit ec: ExecutionContext): Future[Either[PrimeNumberGeneratorServiceError, GetPrimaryNumbersAck]] = {
    logger.debug("Received request: {}", request)

    request.upperBound match {
      case i if i < 2 => FastFuture.successful(Left(IllegalUpperBoundError(i)))
      case _ =>
        Future {
          val start = System.currentTimeMillis()
          val primes = request.algorithm.getOrElse(Iterative).generatePrimes(request.upperBound)
          val end = System.currentTimeMillis()
          logger.debug("Generation of primes took {} ms", end - start)
          Right(GetPrimaryNumbersAck(primes))
        }
    }
  }
}

object PrimeNumberGeneratorService {
  def apply()(implicit system: ActorSystem): PrimeNumberGeneratorServiceLike = new PrimeNumberGeneratorService()
}
