package zinoviev.oleg.primenumbers.web

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import play.api.libs.json.{Format, Json}
import zinoviev.oleg.primenumbers.domain.{GetPrimaryNumbers, GetPrimaryNumbersAck}
import zinoviev.oleg.primenumbers.services.PrimeNumberGeneratorService

class PrimeGeneratorRestApiSpec extends FlatSpec with ScalatestRouteTest with Matchers with BeforeAndAfterAll {
  implicit val as = ActorSystem(getClass.getSimpleName)
  implicit val ec = as.dispatcher

  val service = PrimeNumberGeneratorService()
  val routes = PrimeGeneratorRestApi(service).routes

  override def afterAll(): Unit = as.terminate()

  "PrimeGeneratorRestApi" should "response with result for correct request" in {
    val req = request(GetPrimaryNumbers(10))

    req ~> routes ~> check {
      status shouldBe StatusCodes.OK
      val actual = Json.parse(responseAs[String]).as[GetPrimaryNumbersAck]
      actual shouldBe GetPrimaryNumbersAck(List(2, 3, 5, 7))
    }
  }

  it should "response with BadRequest for incorrect request" in {
    val req = request(GetPrimaryNumbers(0))

    req ~> routes ~> check {
      status shouldBe StatusCodes.BadRequest
    }
  }

  protected def request[T: Format](req: T): HttpRequest = {
    val entity = HttpEntity(ContentType.WithFixedCharset(MediaTypes.`application/json`), Json.stringify(Json.toJson(req)))
    HttpRequest.apply(HttpMethods.POST, Uri("/"), Nil, entity)
  }
}
