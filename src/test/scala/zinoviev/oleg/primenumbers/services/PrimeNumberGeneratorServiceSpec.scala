package zinoviev.oleg.primenumbers.services

import akka.actor.ActorSystem
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, EitherValues, Matchers}
import zinoviev.oleg.primenumbers.domain.{GetPrimaryNumbers, GetPrimaryNumbersAck}

class PrimeNumberGeneratorServiceSpec extends AsyncFlatSpec with Matchers with EitherValues with BeforeAndAfterAll {
  implicit val as = ActorSystem(getClass.getSimpleName)
  implicit val ec = as.dispatcher

  val service = PrimeNumberGeneratorService()

  override def afterAll(): Unit = as.terminate()

  "PrimeNumberGeneratorService" should "fail if upper bound is lower than 2" in {
    val invalidRequest = GetPrimaryNumbers(1)
    service.generate(invalidRequest).map { response =>
      response shouldBe 'left
      response.left.value shouldBe IllegalUpperBoundError(1)
    }
  }

  it should "generate list of primes if request is valid" in {
    val valid = GetPrimaryNumbers(10)
    service.generate(valid).map { response =>
      response shouldBe 'right
      response.right.value shouldBe GetPrimaryNumbersAck(List(2, 3, 5, 7))
    }
  }
}
