import sbtbuildinfo.BuildInfoKeys.{buildInfoKeys, buildInfoOptions}
import sbtbuildinfo.{BuildInfoKey, BuildInfoOption}

version := "0.1"

val akkaV = "2.5.16"
val akkaHttpV = "10.1.4"

val commonSettings = Seq(organization := "zinoviev.oleg", organizationName := "Company", scalaVersion := "2.12.8")

lazy val root = (project in file("."))
  .settings(
    name := "prime-numbers",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpV,
      "com.typesafe.akka" %% "akka-actor" % akkaV,
      "com.typesafe.akka" %% "akka-stream" % akkaV,
      "com.beachape" %% "enumeratum" % "1.5.13",
      "com.typesafe.play" %% "play-json" % "2.6.8",
      "de.heikoseeberger" %% "akka-http-play-json" % "1.20.1",
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV % Test
    ),
    commonSettings
  )
  .enablePlugins(JavaAppPackaging, BuildInfoPlugin)
  .settings(buildInfoKeys := Seq[BuildInfoKey](version), buildInfoOptions += BuildInfoOption.BuildTime)
