# Prime numbers generator

Prime numbers generator service that uses two different algorithms under the hood.

## Requirements
The environment variable which form address:
   * `APPLICATION_HOST`(if not set, `localhost` is used)
   * `APPLICATION_PORT`(if not set, `8080` is used)

## How to run

```shell
$sbt run
```

## Prime numbers generator API

Prime numbers generator application supports three types of the algorithm for generation(`Eratosthenes`, `Recursive`, `Iterative`).
If Algorithm is not set in the request, `Iterative` will be used.

Examples of the requests:
1. with `Eratosthenes` type
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
    "upperBound": 10,
    "algorithm": "Eratosthenes"
}'
```
2. with `Recursive` type
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
    "upperBound": 10,
    "algorithm": "Recursive"
}'
```
3. with `Iterative` type
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
    "upperBound": 10,
    "algorithm": "Iterative"
}'
```
4. without any type(`Iterative` will be used)
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
    "upperBound": 10
}'
```


Example of the response, the field `result` is a field of integers:
```json
{
  "result": [
    2,
    3,
    5,
    7
  ]
}
```

## Restrictions
`upperBound` should not be less then 2